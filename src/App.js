import './App.css';
import Blog2 from './pages/Blog2';
import ContactPageV1 from './pages/ContactPageV1';
import ContactPageV2 from './pages/ContactPageV2';

function App() {
  return (
    <div>
      {/* <Blog2/> */}

      {/* <ContactPageV1/> */}

      <ContactPageV2/>
    </div>
  );
}

export default App;
