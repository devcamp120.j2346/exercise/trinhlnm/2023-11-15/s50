const ContactPageV2 = () => {
    return (
        <div className="relative">
            <div className="bg-[#E9F9FF] pl-[50px] flow-root">
                <div className="mt-[200px] mb-[25px] text-[#000] font-inter text-[15px] font-[600] leading-[20px]">CONTACT US</div>
                <div className="mb-[70px] text-[#000] font-inter text-[35px] font-[600] leading-[40px]">Let’s talk about<br />your problem.</div>
            </div>
            <div className="flex w-[60%] px-[50px] pt-[80px] justify-center">
                <div className="flex w-[50%]">
                    <img src={require("../assets/images/map-marker1.png")} className="w-[35px] h-[35px] mr-[25px]" />
                    <div>
                        <div className="text-[#212B36] font-inter text-[18px] font-[600] leading-[24px]">Our Location</div>
                        <div className="text-[#637381] font-inter text-[16px] font-[400] leading-[24px]">401 Broadway, 24th Floor, Orchard Cloud View, London</div>
                    </div>
                </div>
                <div className="flex w-[45%]">
                    <img src={require("../assets/images/envelope.png")} className="w-[35px] h-[35px] mr-[25px]" />
                    <div>
                        <div className="text-[#212B36] font-inter text-[18px] font-[600] leading-[24px]">How Can We Help?</div>
                        <div className="text-[#637381] font-inter text-[16px] font-[400] leading-[24px]">info@yourdomain.com</div>
                        <div className="text-[#637381] font-inter text-[16px] font-[400] leading-[24px]">contact@yourdomain.com</div>
                    </div>
                </div>
            </div>
            <div className="absolute top-0 right-0 w-[40%] py-[50px] pr-[50px]">        
                <div className="rounded-[8px] bg-[#FFF] drop-shadow-[0px_4px_28px_rgba(0,0,0,0.05)] p-[60px]">
                <div className="mb-[30px] text-[#000] font-inter text-[28px] font-[600] leading-[35px]">Send us a Message</div>
                    <div>
                        <div className="text-[#637381] font-inter text-[13px] font-[400] leading-[20px]">Full Name*</div>
                        <input
                            style={{ borderBottom: "1px solid #F1F1F1" }}
                            className="rounded-[5px] border-0 py-[15px] w-full mb-[25px]
                        text-[#637381] font-inter text-[15px] font-[400] leading-[20px]"
                            value="Adam Gelius"
                        />
                    </div>
                    <div>
                        <div className="text-[#637381] font-inter text-[13px] font-[400] leading-[20px]">Email*</div>
                        <input
                            style={{ borderBottom: "1px solid #F1F1F1" }}
                            className="rounded-[5px] border-0 py-[15px] w-full mb-[25px]
                        text-[#637381] font-inter text-[15px] font-[400] leading-[20px]"
                            value="example@yourmail.com"
                        />
                    </div>
                    <div>
                        <div className="text-[#637381] font-inter text-[13px] font-[400] leading-[20px]">Phone*</div>
                        <input
                            style={{ borderBottom: "1px solid #F1F1F1" }}
                            className="rounded-[5px] border-0 py-[15px] w-full mb-[25px]
                        text-[#637381] font-inter text-[15px] font-[400] leading-[20px]"
                            value="+885 1254 5211 552"
                        />
                    </div>
                    <div>
                        <div className="text-[#637381] font-inter text-[13px] font-[400] leading-[20px]">Messege*</div>
                        <input
                            style={{ borderBottom: "1px solid #F1F1F1" }}
                            className="rounded-[5px] border-0 py-[15px] w-full mb-[25px]
                        text-[#637381] font-inter text-[15px] font-[400] leading-[20px]"
                            value="type your message here"
                        />
                    </div>
                    <button className="bg-[#3056D3] rounded-[5px] py-[12px] px-[40px] text-[#FFF] font-inter text-[16px] font-[500] leading-[20px]">Send</button>
                </div>
            </div>
        </div>
    );
}

export default ContactPageV2;