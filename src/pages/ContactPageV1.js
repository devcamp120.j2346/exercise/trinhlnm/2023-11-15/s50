import "../App.css";

const ContactPageV1 = () => {
    return (
        <div className="container mx-auto flex">
            <div className="w-[40%]">
                <div className="text-[#3056D3] font-inter text-[16px] font-[600] leading-[20px] mt-[100px] mb-[10px]">Contact Us</div>
                <div className="text-[#212B36] font-inter text-[40px] font-[700] leading-[45px] mb-[30px]">Get In Touch With Us</div>
                <div className="text-[#637381] font-inter text-[16px] font-[400] leading-[27px] mb-[40px]">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eius tempor incididunt ut labore et dolore magna aliqua. Ut enim adiqua minim veniam quis nostrud exercitation ullamco
                </div>

                <div className="flex items-center mb-[35px]">
                    <img src={require("../assets/images/Frame36.png")} className="w-[70px] mr-[24px]" />
                    <div>
                        <div className="text-[#000] font-inter text-[20px] font-[600] leading-[27px]">Our Location</div>
                        <div className="text-[#637381] font-inter text-[16px] font-[500] leading-[25px]">99 S.t Jomblo Park Pekanbaru 28292. Indonesia</div>
                    </div>
                </div>

                <div className="flex items-center mb-[35px]">
                    <img src={require("../assets/images/Frame37.png")} className="w-[70px] mr-[24px]" />
                    <div>
                        <div className="text-[#000] font-inter text-[20px] font-[600] leading-[27px]">Phone Number</div>
                        <div className="text-[#637381] font-inter text-[16px] font-[500] leading-[25px]">(+62)81 414 257 9980</div>
                    </div>
                </div>

                <div className="flex items-center mb-[100px]">
                    <img src={require("../assets/images/Frame38.png")} className="w-[70px] mr-[24px]" />
                    <div>
                        <div className="text-[#000] font-inter text-[20px] font-[600] leading-[27px]">Email Address</div>
                        <div className="text-[#637381] font-inter text-[16px] font-[500] leading-[25px]">info@yourdomain.com</div>
                    </div>
                </div>
            </div>

            <div className="w-[60%] py-[50px] pl-[50px] flex justify-center items-center">
                <div className="py-[50px] px-[45px] bg">
                    <div className="rounded-[8px] bg-[#FFF] drop-shadow-[0px_4px_18px_rgba(0,0,0,0.07)] p-[50px]">
                        <input
                            className="rounded-[5px] border border-solid border-[#F0F0F0] bg-[#FFF] p-[15px] w-full mb-[25px]
                        text-[#637381] font-inter text-[15px] font-[400] leading-[20px]"
                            placeholder="Your Name"
                        />
                        <input
                            className="rounded-[5px] border border-solid border-[#F0F0F0] bg-[#FFF] p-[15px] w-full mb-[25px]
                        text-[#637381] font-inter text-[15px] font-[400] leading-[20px]"
                            placeholder="Your Email"
                        />
                        <input
                            className="rounded-[5px] border border-solid border-[#F0F0F0] bg-[#FFF] p-[15px] w-full mb-[25px]
                        text-[#637381] font-inter text-[15px] font-[400] leading-[20px]"
                            placeholder="Your Phone"
                        />
                        <input
                            className="rounded-[5px] border border-solid border-[#F0F0F0] bg-[#FFF] p-[15px] w-full mb-[25px]
                        text-[#637381] font-inter text-[15px] font-[400] leading-[20px] h-[10rem]"
                            placeholder="Your Message"
                        />
                        <button className="bg-[#3056D3] rounded-[5px] w-full p-[15px] text-[#FFF] font-roboto text-[16px] font-[400] leading-[20px]">Send Message</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ContactPageV1;