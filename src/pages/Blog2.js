const Blog2 = () => {
    return (
        <>
            <div className="text-[#3056D3] text-center text-[18px] font-semibold leading-[24px] mt-[120px]">
                Our Blogs
            </div>
            <div className="text-[#2E2E2E] text-center text-[40px] font-bold leading-[45px] capitalize mt-[8px]">
                Our Recent News
            </div>
            <div className="text-[#637381] text-center text-[15px] font-normal leading-[25px] mt-[15px]">
                There are many variations of passages of Lorem Ipsum available <br /> but the majority have suffered alteration in some form.
            </div>

            <div className="container mx-auto mt-[80px] mb-[120px]">
                <div className="grid grid-cols-3 gap-4">
                    <div className="col-span-2 flex">
                        <img src={require("../assets/images/Rectangle4367.jpg")} className="w-2/5" />
                        <div className="bg-[#F4F7FF] p-[30px]">
                            <div className="mb-[7px] text-[#3056D3] font-inter text-[14px] font-semibold leading-5 tracking-[0.5px]">Digital Marketing</div>
                            <div className="mb-[12px] text-[#212B36] font-inter text-[22px] font-semibold leading-[30px]">How to use Facebook ads to sell online courses</div>
                            <div className="mb-[30px] text-[#637381] font-inter text-[16px] font-[400] leading-[25px]">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                            <div className="text-[#637381] font-inter text-[14px] font-[500] leading-[20px] tracking-[0.5px]">Read More &rarr;</div>
                        </div>
                    </div>
                    <div className="col-span-2 flex">
                        <img src={require("../assets/images/Rectangle4368.jpg")} className="w-2/5" />
                        <div className="bg-[#F4F7FF] p-[30px]">
                            <div className="mb-[7px] text-[#3056D3] font-inter text-[14px] font-semibold leading-5 tracking-[0.5px]">Digital Marketing</div>
                            <div className="mb-[12px] text-[#212B36] font-inter text-[22px] font-semibold leading-[30px]">The Data-Driven Approach to Understanding.</div>
                            <div className="mb-[30px] text-[#637381] font-inter text-[16px] font-[400] leading-[25px]">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                            <div className="text-[#637381] font-inter text-[14px] font-[500] leading-[20px] tracking-[0.5px]">Read More &rarr;</div>
                        </div>
                    </div>
                    <div className="row-start-1 row-span-2 col-start-3 bg-[#3056D3] p-[30px]">
                        <div>
                            <div className="mb-[7px] text-[#FFF] font-inter text-[14px] font-[600] leading-[20px] tracking-[0.5px]">Graphics Design</div>
                            <div className="mb-[12px] text-[#FFF] font-inter text-[22px] font-[600] leading-[30px]">Design is a Plan or The Construction of an Object.</div>
                            <div className="mb-[30px] text-[#EBEBEB] font-inter text-[16px] font-[400] leading-[25px]">Lorem Ipsum is simply dummy text of the printing and industry page when looking at its layout.</div>
                            <div className="text-[#EBEBEB] font-inter text-[14px] font-[500] leading-[20px] tracking-[0.5px]">Read More &rarr;</div>
                        </div>
                        <div style={{height: "1px", width: "100%", backgroundColor: "rgba(255, 255, 255, 0.29)", margin: "25px auto"}}></div>
                        <div>
                            <div className="mb-[7px] text-[#FFF] font-inter text-[14px] font-[600] leading-[20px] tracking-[0.5px]">Digital Marketing</div>
                            <div className="mb-[12px] text-[#FFF] font-inter text-[22px] font-[600] leading-[30px]">How to use Facebook ads to sell online courses</div>
                            <div className="mb-[30px] text-[#EBEBEB] font-inter text-[16px] font-[400] leading-[25px]">Lorem Ipsum is simply dummy text of the printing and industry page when looking at its layout.</div>
                            <div className="text-[#EBEBEB] font-inter text-[14px] font-[500] leading-[20px] tracking-[0.5px]">Read More &rarr;</div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Blog2;